<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Karyawan extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['karyawan'] = $this->Karyawan_model->getKaryawan();

        $this->load->view('templates/Header');
        $this->load->view('pages/Karyawan', $data);
        $this->load->view('templates/Footer');
    }

    public function isiNilai($kd = null)
    {
        $data['karyawan'] = $this->Karyawan_model->getKaryawanByKd($kd);
        $data['kriteria'] = $this->Kriteria_model->getKriteria();
        $data['nilai'] = $this->Karyawan_model->getNilai($kd);

        $this->load->view('templates/Header');
        $this->load->view('pages/IsiNilai', $data);
        $this->load->view('templates/Footer');
    }

    public function tambah()
    {
        $this->load->view('templates/Header');
        $this->load->view('pages/TambahKaryawan');
        $this->load->view('templates/Footer');
    }

    public function ubah($kd = null)
    {
        $data['karyawan'] = $this->Karyawan_model->getKaryawanByKd($kd);
        $this->load->view('templates/Header');
        $this->load->view('pages/UbahKaryawan', $data);
        $this->load->view('templates/Footer');
    }

    public function simpan()
    {
        if ($this->Karyawan_model->simpan()) {
            $this->session->set_flashdata('success', 'Berhasil Menyimpan Data');
        } else {
            $this->session->set_flashdata('error', 'Gagal Menyimpan Data');
        }
        redirect('Karyawan');
    }

    public function update()
    {
        if ($this->Karyawan_model->update()) {
            $this->session->set_flashdata('success', 'Berhasil Mengubah Data');
        } else {
            $this->session->set_flashdata('error', 'Gagal Mengubah Data');
        }
        redirect('Karyawan');
    }

    public function hapus($kd = null)
    {
        $this->Karyawan_model->hapus($kd);
        $this->session->set_flashdata('success', 'Berhasil Menghapus Data');
        redirect('Karyawan');
    }
}
