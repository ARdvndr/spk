<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Penilaian extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }


    public function WP()
    {
        $data['WP'] = $this->Penilaian_model->getWP();
        $data['kriteria'] = $this->Kriteria_model->getKriteria();

        $this->load->view('templates/Header');
        $this->load->view('pages/PenilaianWP', $data);
        $this->load->view('templates/Footer');
    }

    public function SAW()
    {
        $data['SAW'] = $this->Penilaian_model->getSAW();
        $this->load->view('templates/Header');
        $this->load->view('pages/PenilaianSAW', $data);
        $this->load->view('templates/Footer');
    }
}
