<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kriteria extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['kriteria'] = $this->Kriteria_model->getKriteria();

        $this->load->view('templates/Header');
        $this->load->view('pages/Kriteria', $data);
        $this->load->view('templates/Footer');
    }

    public function tambah()
    {
        $this->load->view('templates/Header');
        $this->load->view('pages/TambahKriteria');
        $this->load->view('templates/Footer');
    }

    public function ubah($kd = null)
    {
        $data['kriteria'] = $this->Kriteria_model->getKriteriaByKd($kd);
        $this->load->view('templates/Header');
        $this->load->view('pages/UbahKriteria', $data);
        $this->load->view('templates/Footer');
    }

    public function simpan()
    {
        if ($this->Kriteria_model->simpan()) {
            $this->session->set_flashdata('success', 'Berhasil Menyimpan Data');
        } else {
            $this->session->set_flashdata('error', 'Gagal Menyimpan Data');
        }
        redirect('Kriteria');
    }

    public function update()
    {
        if ($this->Kriteria_model->update()) {
            $this->session->set_flashdata('success', 'Berhasil Mengubah Data');
        } else {
            $this->session->set_flashdata('error', 'Gagal Mengubah Data');
        }
        redirect('Kriteria');
    }

    public function hapus($kd = null)
    {
        $this->Kriteria_model->hapus($kd);
        $this->session->set_flashdata('success', 'Berhasil Menghapus Data');
        redirect('Kriteria');
    }
}
