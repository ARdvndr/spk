<?php
$content = array(
    0 => array("nama" => "Kriteria", "color" => "blue", "href" => base_url('Kriteria'), "class" => "fas fa-file-alt"),
    1 => array("nama" => "Karyawan", "color" => "light-orange", "href" => base_url('Karyawan'), "class" => "fas fa-id-card"),
    2 => array("nama" => "Penilaian WP", "color" => "color", "href" => base_url('Penilaian/WP'), "class" => "fas fa-calculator"),
    3 => array("nama" => "Penilaian SAW", "color" => "purple", "href" => base_url('Penilaian/SAW'), "class" => "fas fa-calculator"),
    // 4 => array("nama" => "Pengguna", "color" => "red", "href" => base_url('User'), "class" => "fas fa-users"),
);
?>

<div class="container pt-5">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">SPK</h1>
        </div>
    </div>

    <div class="mainbody-section text-center">
        <div class="row">

            <?php
            foreach ($content as $val) {
                echo "
                <div class='card card-xl ml-4 mb-4 menu-item' style='width: 15rem;'>
                    <a href='$val[href]' class='btn btn-lg $val[color]'>
                        <div class='card-body'>
                            <i class='$val[class]'></i>
                            <h5 class='card-title'>$val[nama]</h5>
                        </div>
                    </a>
                </div>";
            }
            ?>

        </div>
    </div>
</div>