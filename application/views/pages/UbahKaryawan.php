<div class="container pt-3">
    <div class="row">

        <div class="col-md-6">
            <div class="card">
                <form action="<?= base_url('Karyawan/update'); ?>" method="post">
                    <input type="hidden" name="kd" value="<?= $karyawan['kd_karyawan'] ?>">

                    <div class=" card-header">
                        <div class="col-md-12">
                            <h2 class="page-header">Tambah Karyawan</h2>
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="form-group row">
                            <label for="inputNamaKaryawan" class="col-sm-3 col-form-label">Nama</label>
                            <div class="col-sm-9">
                                <input type="text" name="nama" class="form-control" id="inputNamaKaryawan" placeholder="Nama" value="<?= $karyawan['nama'] ?>" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inputAlamatKaryawan" class="col-sm-3 col-form-label">Alamat</label>
                            <div class="col-sm-9">
                                <textarea name="alamat" id="inputAlamatKaryawan" class="form-control" cols="30" rows="3" required><?= $karyawan['alamat'] ?></textarea>
                            </div>
                        </div>

                        <fieldset class="form-group">
                            <div class="row">
                                <legend class="col-form-label col-sm-3 pt-0">Jenis Kelamin</legend>
                                <div class="col-sm-9">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="jkel" id="jkel1" value="Laki-Laki" required <?php if ($karyawan['jkel'] == 'Laki-Laki') echo "checked"; ?>>
                                        <label class="form-check-label" for="jkel1">
                                            Laki-Laki
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="jkel" id="jkel2" value="Perempuan" required <?php if ($karyawan['jkel'] == 'Perempuan') echo "checked"; ?>>
                                        <label class="form-check-label" for="jkel2">
                                            Perempuan
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <div class="form-group row">
                            <label for="inputTelepon" class="col-sm-3 col-form-label">No Telepon</label>
                            <div class="col-sm-4">
                                <input type="text" name="tlp" class="form-control" id="inputTelepon" placeholder="No Telepon" value="<?= $karyawan['tlp'] ?>" required>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer">
                        <div class="form-row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <a href="<?= base_url('Karyawan'); ?>" class="btn btn-danger">Batal</a>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>