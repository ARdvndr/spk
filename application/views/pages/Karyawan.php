<div class="container pt-3">
    <div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">Data Karyawan</h2>
        </div>
    </div>

    <div class="card">
        <div class="card-header bg-dark">
            <a href="<?= base_url('Karyawan/Tambah'); ?>" class="btn btn-sm btn-info">
                <i class="fas fa-plus"></i> Tambah Karyawan
            </a>
        </div>

        <div class="card-body">
            <table class="table table-sm table-striped" id="tableKaryawan" style="width:100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Kode Karyawan</th>
                        <th>Nama Karyawan</th>
                        <th>Alamat</th>
                        <th>No. Tlp</th>
                        <th>Jenis Kelamin</th>
                        <th>Nilai</th>
                        <th>Aksi</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    $no = 1;
                    foreach ($karyawan as $k) :
                    ?>
                        <tr>
                            <td><?= $no++ ?></td>
                            <td><?= $k['kd_karyawan']; ?></td>
                            <td><?= $k['nama']; ?></td>
                            <td><?= $k['alamat']; ?></td>
                            <td><?= $k['tlp']; ?></td>
                            <td><?= $k['jkel']; ?></td>
                            <td><a href="<?= base_url('Karyawan/isiNilai/' . $k['kd_karyawan']); ?>" class="btn btn-sm btn-primary">Isi</a></td>
                            <td>
                                <a class="btn btn-sm btn-warning" href="<?= base_url('Karyawan/ubah/' . $k['kd_karyawan']) ?>"> Edit <i class="fas fa-edit"></i></a>
                                <button type="button" class="btn btn-sm btn-danger hapusKaryawan" data-id="<?= $k['kd_karyawan']; ?>"> Hapus <i class="fas fa-trash"></i></button>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    $('#tableKaryawan').DataTable({
        order: [
            [1, 'asc']
        ],
        columnDefs: [{
            targets: [0, 6],
            orderable: false,
            className: 'text-center',
            width: '8%',
        }, {
            targets: [7],
            orderable: false,
            className: 'text-center',
            width: '15%',
        }]
    });

    $('.hapusKaryawan').on('click', function() {
        var kd = $(this).attr('data-id');

        alertify.confirm("Yakin Menghapus Data Ini?", function() {
            window.location.href = "<?= base_url('Karyawan/hapus/') ?>" + kd;
        }, function() {
            alertify.error('Batal')
        })
    });
</script>