<div class="container pt-3">
    <div class="row">

        <div class="col-md-6">
            <div class="card">
                <form action="<?= base_url('Kriteria/simpan'); ?>" method="post">

                    <div class="card-header">
                        <div class="col-md-12">
                            <h2 class="page-header">Tambah Kriteria</h2>
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="form-group row">
                            <label for="inputKodeKriteria" class="col-sm-3 col-form-label">Kode Kriteria</label>
                            <div class="col-sm-4">
                                <input type="text" name="kd" class="form-control" id="inputKodeKriteria" placeholder="Kode Kriteria" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inputNamaKriteria" class="col-sm-3 col-form-label">Nama Kriteria</label>
                            <div class="col-sm-9">
                                <input type="text" name="nama" class="form-control" id="inputNamaKriteria" placeholder="Nama Kriteria" required>
                            </div>
                        </div>

                        <fieldset class="form-group">
                            <div class="row">
                                <legend class="col-form-label col-sm-3 pt-0">Jenis</legend>
                                <div class="col-sm-9">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="jenis" id="jenis1" value="Keuntungan" required>
                                        <label class="form-check-label" for="jenis1">
                                            Keuntungan
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="jenis" id="jenis2" value="Biaya" required>
                                        <label class="form-check-label" for="jenis2">
                                            Biaya
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <div class="form-group row">
                            <label for="inputBobot" class="col-sm-3 col-form-label">Bobot</label>
                            <div class="col-sm-4">
                                <input type="number" name="bobot" class="form-control" id="inputBobot" placeholder="Bobot" required>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer">
                        <div class="form-row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <a href="<?= base_url('Kriteria'); ?>" class="btn btn-danger">Batal</a>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>