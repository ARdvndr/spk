<?php print_r($WP); ?>

<div class="container pt-3">
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">Penilaian WP</h3>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-sm table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Alternatif</th>
                                <?php foreach ($kriteria as $k) { ?>
                                    <th><?= $k['nama_kriteria']; ?></th>
                                <?php } ?>
                            </tr>
                        </thead>

                        <tbody>
                            <?php
                            $no = 1;
                            foreach ($WP as $w) { ?>
                                <tr>
                                    <td><?= $no++ ?></td>
                                    <td><?= $w['kd'] . ' - ' . $w['nama'] ?></td>
                                    <?php foreach ($kriteria as $k) { ?>
                                        <td><?= $w[$k['kd_kriteria']]; ?></td>
                                    <?php } ?>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">

                </div>
            </div>
        </div>
    </div>
</div>