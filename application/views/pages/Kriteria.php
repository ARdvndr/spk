<div class="container pt-3">
    <div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">Data Kriteria</h2>
        </div>
    </div>

    <div class="card">
        <div class="card-header bg-dark">
            <a href="<?= base_url('Kriteria/tambah'); ?>" class="btn btn-sm btn-info">
                <i class="fas fa-plus"></i> Tambah Kriteria
            </a>
        </div>

        <div class="card-body">
            <table class="table table-sm table-striped" id="tableKriteria" style="width:100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Kode Kriteria</th>
                        <th>Nama Kriteria</th>
                        <th>Jenis</th>
                        <th>Bobot</th>
                        <th>Aksi</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    $no = 1;
                    foreach ($kriteria as $k) :
                    ?>
                        <tr>
                            <td><?= $no++ ?></td>
                            <td><?= $k['kd_kriteria']; ?></td>
                            <td><?= $k['nama_kriteria']; ?></td>
                            <td><?= $k['jenis']; ?></td>
                            <td><?= $k['bobot']; ?></td>
                            <td>
                                <a class="btn btn-sm btn-warning" href="<?= base_url('Kriteria/ubah/' . $k['kd_kriteria']) ?>"> Edit <i class="fas fa-edit"></i></a>
                                <button type="button" class="btn btn-sm btn-danger hapusKriteria" data-id="<?= $k['kd_kriteria']; ?>"> Hapus <i class="fas fa-trash"></i></button>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    $('#tableKriteria').DataTable({
        order: [
            [1, 'asc']
        ],
        columnDefs: [{
                targets: [0, 4],
                orderable: false,
                className: 'text-center',
                width: '10%',
            },
            {
                targets: [5],
                orderable: false,
                className: 'text-center',
                width: '15%',
            }
        ]
    });

    $('.hapusKriteria').on('click', function() {
        var kd = $(this).attr('data-id');

        alertify.confirm("Yakin Menghapus Data Ini?", function() {
            window.location.href = "<?= base_url('Kriteria/hapus/') ?>" + kd;
        }, function() {
            alertify.error('Batal')
        })
    });
</script>