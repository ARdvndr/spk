<div class="container pt-5">
    <div class="row">
        <div class="col-md-9">

            <div class="card">
                <form action="" method="post">

                    <div class="card-header col-md-12">
                        <h3 class="profile-username"><?= $karyawan['kd_karyawan'] . ' - ' . $karyawan['nama'] ?></h3>
                    </div>

                    <div class="card-body">
                        <div class="row">

                            <div class="col-md-9">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th style="width: 50px;" class="text-center">No.</th>
                                            <th style="width: 200px;">Kriteria</th>
                                            <th style="width: 150px;">Nilai</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php
                                        $no = 1;
                                        foreach ($kriteria as $k) :
                                        ?>
                                            <tr>
                                                <td class="text-center"><?= $no++; ?></td>
                                                <td><?= $k['kd_kriteria'] . ' - ' . $k['nama_kriteria']; ?></td>
                                                <td>
                                                    <input type="number" class="form-control" name="<?= $k['kd_kriteria']; ?>" id="<?= $k['kd_kriteria']; ?>" <?php
                                                                                                                                                                foreach ($nilai as $n) {
                                                                                                                                                                    if ($n['kd_kriteria'] == $k['kd_kriteria']) {
                                                                                                                                                                        echo 'value="' . $n['nilai'] . '"';
                                                                                                                                                                    }
                                                                                                                                                                }
                                                                                                                                                                ?>>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>

                            <div class="col-md-3">
                                <div class="box-profile">
                                    <input type="submit" class="btn btn-primary btn-block btn-sm" value="Simpan"></input>
                                    <a href="<?= base_url('Karyawan'); ?>" class="btn btn-sm btn-block btn-danger">Batal</a>
                                </div>
                            </div>

                        </div>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>