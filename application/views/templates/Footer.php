<script type="text/javascript">
    //override defaults
    alertify.defaults.theme.ok = "btn btn-primary";
    alertify.defaults.theme.cancel = "btn btn-danger";
    alertify.defaults.theme.input = "form-control";
    alertify.set('notifier', 'position', 'top-center');

    <?php if (!empty($this->session->flashdata('error'))) { ?>
        alertify.notify("<?= $this->session->flashdata('error'); ?>", "error");
    <?php } else if (!empty($this->session->flashdata('success'))) { ?>
        alertify.notify("<?= $this->session->flashdata('success'); ?>", "success");
    <?php } ?>
</script>
</body>

</html>