<nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top">
    <div class="container">

        <a class="navbar-brand" href="<?= base_url() ?>">Dashboard</a>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="<?= base_url('Penilaian/WP'); ?>">
                    <span class="fa fa-calculator" aria-hidden="true"></span> WP
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= base_url('Penilaian/SAW'); ?>">
                    <span class="fa fa-calculator" aria-hidden="true"></span> SAW
                </a>
            </li>
        </ul>

        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link btn btn-danger btn-sm text-white" href="<?php echo base_url('Logout') ?>">
                    <span class="fa fa-sign-out-alt"></span> Logout
                </a>
            </li>
        </ul>

    </div>
</nav>