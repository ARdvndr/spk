<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Penilaian_model extends CI_Model
{

    public function getNilai($kd)
    {
        $data = $this->db
            ->select('kd_kriteria, nilai')
            ->where('kd_karyawan', $kd)
            ->get('nilai')
            ->result_array();

        return $data;
    }

    public function getTotalBobot()
    {
        $d = $this->db
            ->select('sum(bobot) as totalBobot')
            ->get('kriteria')
            ->row();

        $ttlB = intval($d->totalBobot);
        return $ttlB;
    }

    public function getSAW()
    {
        $kriteria = $this->Kriteria_model->getKriteria();
        $karyawan = $this->Karyawan_model->getKaryawan();

        $data = [];
        $max = [];
        $min = [];

        //Nilai Karyawan
        $i = 0;
        foreach ($karyawan as $ka) {
            $data[$i]['kd'] = $ka['kd_karyawan'];
            $data[$i]['nama'] = $ka['nama'];
            $n = $this->getNilai($ka['kd_karyawan']);
            foreach ($n as $n) {
                $data[$i][$n['kd_kriteria']] = intval($n['nilai']);
            }
            $i++;
        }

        //Normalisasi Bobot
        $nBobot = [];
        foreach ($kriteria as $k) {
            $bb = $k['bobot'];
            $nBobot[$k['kd_kriteria']] = round($bb, 4);
        }

        //Mencari max & min
        foreach ($kriteria as $k) {
            $temp = [];
            for ($i = 0; $i < count($data); $i++) {
                $temp[] = $data[$i][$k['kd_kriteria']];
            }
            $max[$k['kd_kriteria']] = max($temp);
            $min[$k['kd_kriteria']] = min($temp);
        }


        //Normalisasi Alternatif
        for ($i = 0; $i < count($data); $i++) {
            foreach ($kriteria as $k) {
                $x = $data[$i][$k['kd_kriteria']];
                $nMax = $max[$k['kd_kriteria']];
                $nMin = $min[$k['kd_kriteria']];
                if ($k['jenis'] == 'Keuntungan') {
                    $hsl = $x / $nMax;
                } else {
                    $hsl = $nMin / $x;
                }
                $data[$i]['n' . $k['kd_kriteria']] = round($hsl, 4);
            }
        }

        //Perangkingan
        for ($i = 0; $i < count($data); $i++) {
            $sum = 0;
            foreach ($kriteria as $k) {
                $x = $data[$i]['n' . $k['kd_kriteria']];
                $y = $nBobot[$k['kd_kriteria']];
                $hsl = $x * $y;
                $sum = $sum + $hsl;
            }
            $data[$i]['total'] = round($sum, 4);
        }

        return $data;
    }

    public function getWP()
    {
        $kriteria = $this->Kriteria_model->getKriteria();
        $karyawan = $this->Karyawan_model->getKaryawan();
        $totalBobot = $this->getTotalBobot();
        $data = [];

        //Nilai Karyawan
        $i = 0;
        foreach ($karyawan as $ka) {
            $data[$i]['kd'] = $ka['kd_karyawan'];
            $data[$i]['nama'] = $ka['nama'];
            $n = $this->getNilai($ka['kd_karyawan']);
            foreach ($n as $n) {
                $data[$i][$n['kd_kriteria']] = intval($n['nilai']);
            }
            $i++;
        }


        //Normalisasi Bobot
        $nBobot = [];
        foreach ($kriteria as $k) {
            $bb = $k['bobot'] / $totalBobot;
            if ($k['jenis'] == 'Biaya') $bb = $bb * (-1);
            $nBobot[$k['kd_kriteria']] = round($bb, 4);
        }


        //Mencari S
        $sumS = 0;
        for ($i = 0; $i < count($data); $i++) {
            $s = 1;
            foreach ($kriteria as $k) {
                if (!isset($data[$i][$k['kd_kriteria']])) {
                    $this->session->set_flashdata('error', 'Data Nilai Belum Terisi Lengkap');
                    redirect(base_url('Karyawan'));
                }

                $x = $data[$i][$k['kd_kriteria']];
                $y = $nBobot[$k['kd_kriteria']];
                $hsl = pow($x, $y);
                $s = $s * $hsl;
            }
            $s = round($s, 4);
            $sumS = $sumS + $s;
            $data[$i]['s'] = $s;
        }

        //Mencari V
        for ($i = 0; $i < count($data); $i++) {
            $v = $data[$i]['s'] / $sumS;
            $data[$i]['v'] = round($v, 4);
        }

        return $data;
    }
}
