<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kriteria_model extends CI_Model
{

    public function getKriteria()
    {
        $data = $this->db
            ->get('kriteria')
            ->result_array();
        return $data;
    }

    public function getKriteriaByKd($kd)
    {
        $data = $this->db
            ->get_where('kriteria', array('kd_kriteria', $kd))
            ->row_array();

        return $data;
    }

    public function simpan()
    {
        $post = $this->input->post();
        $this->kd_kriteria = $post["kd"];
        $this->nama_kriteria = $post["nama"];
        $this->jenis = $post["jenis"];
        $this->bobot = $post["bobot"];

        return $this->db->insert('kriteria', $this);
    }

    public function update()
    {
        $post = $this->input->post();
        $this->nama_kriteria = $post["nama"];
        $this->jenis = $post["jenis"];
        $this->bobot = $post["bobot"];

        return $this->db->update('kriteria', $this, array('kd_kriteria' => $post['kd']));
    }

    public function hapus($kd)
    {
        return $this->db->delete('kriteria', array('kd_kriteria' => $kd));
    }
}
