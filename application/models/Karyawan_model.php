<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Karyawan_model extends CI_Model
{

    public function getKaryawan()
    {
        $data = $this->db
            ->get('karyawan')
            ->result_array();

        return $data;
    }

    public function getKaryawanByKd($kd)
    {
        $data = $this->db
            ->get_where('karyawan', array('kd_karyawan' => $kd))
            ->row_array();

        return $data;
    }

    public function getKd()
    {
        $q = $this->db->query("SELECT MAX(RIGHT(kd_karyawan ,3)) AS kd_max FROM karyawan");
        $kd = "";
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $k) {
                $tmp = ((int)$k->kd_max) + 1;
                $kd = sprintf("%03s", $tmp);
            }
        } else {
            $kd = "001";
        }
        return "K" . $kd;
    }

    public function simpan()
    {
        $post = $this->input->post();
        $this->kd_karyawan = $this->getKd();
        $this->nama = $post["nama"];
        $this->alamat = $post["alamat"];
        $this->tlp = $post["tlp"];
        $this->jkel = $post["jkel"];

        return $this->db->insert('karyawan', $this);
    }

    public function update()
    {
        $post = $this->input->post();
        $this->nama = $post["nama"];
        $this->alamat = $post["alamat"];
        $this->tlp = $post["tlp"];
        $this->jkel = $post["jkel"];

        return $this->db->update('karyawan', $this, array('kd_karyawan' => $post['kd']));
    }

    public function hapus($kd)
    {
        return $this->db->delete('karyawan', array('kd_karyawan' => $kd));
    }

    public function getNilai($kd)
    {
        $data = $this->db
            ->where('kd_karyawan', $kd)
            ->get('nilai')
            ->result_array();

        return $data;
    }

    public function saveNilai()
    {
        # code...
    }
}
